#ifndef Algorithms_H
#define Algorithms_H

#include <stdlib.h>
#include <time.h>

// <<< SEARCH ALGORITHMS >>>

// this algorithm operates on an array sorted in an escending order;
// returnFirstMatch returns the first matched occurance (if true, this disables returnFirstOcc option);
// returnFirstOcc flags the return of first or last occurance in ascending order (returnFirstMatch must be false)
template<typename T>
int binarySearch(T containerArray[], int sizeOfContainer, T soughtValue,
	bool returnFirstMatch = true, bool returnFirstOcc = true) {

	int low = 0;
	int high = sizeOfContainer - 1;
	int result = -1;

	while (low <= high) {

		int mid = low + (high - low) / 2; //avoiding wrapping of int values around

		if (containerArray[mid] == soughtValue) {
			result = mid;
			if (returnFirstMatch) break; // just return the first found ("random") value and don't bother about occurance
										 // order
			else {
				if (returnFirstOcc) high = mid - 1;//continue searching for the first occurance (don't return a random one)
				else low = mid + 1;//continue searching for the last occrance (don't return a random one)
			}
		}
		else if (containerArray[mid] < soughtValue)
			low = mid + 1;
		else
			high = mid - 1;
	}

	return result;
}

//<<< SORTING ALGORITHMS >>>

//quick sort
// low and high are inclusive
int randomRange(int low, int high) {

	return (rand() % (high + 1)) + low;
}

template <typename T>
void swap(T& a, T& b) {
	T tmp = a;
	a = b;
	b = tmp;
}

//start and end indices are incusive
template <typename T>
int randomPartition(T arrayContainer[], int startIndex, int endIndex) {// this is not random now

	int pivotIndex = endIndex;
	int partitionIndex = startIndex;

	for (int i = startIndex; i < endIndex; i++) {
		if (arrayContainer[i] <= arrayContainer[pivotIndex])
		{
			swap<T>(arrayContainer[i], arrayContainer[partitionIndex]);
			partitionIndex++;
		}
	}
	swap<T>(arrayContainer[endIndex], arrayContainer[partitionIndex]);
	return partitionIndex;
}

template<typename T>
void quickSort(T arrayContainer[], int startIndex, int endIndex) {//array is passed as pointer by default

	if (startIndex < endIndex) {
		// random partitioning eliminates a great chance for the the worst time comlexity of n^2
		int partitionIndex = randomPartition<T>(arrayContainer, startIndex, endIndex);//array is passed as pointer by default
		quickSort(arrayContainer, startIndex, partitionIndex - 1);
		quickSort(arrayContainer, partitionIndex + 1, endIndex);
	}
}

#endif // !Algorithms_H


