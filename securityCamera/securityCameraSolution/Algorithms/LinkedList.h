
#ifndef  LinkedList_H
#define LinkedList_H

#include <iostream>
#include <functional>
#include <string>

//Linked List
//TODO: Handle copying. assignment and deletion of list (right now memory is leaking)

template <typename T>
class LinkedList {
public:

	LinkedList();

	void InsertFront(const T& data);
	void Insert(int index, const T& data);// T by copy or by reference ?
	void InsertEnd(const T& data);
	void Delete(int index);
	bool Delete(std::function <bool(T&)> f);
    int Search(std::function <bool(T&)> f) const;
	void Print() const;
	bool Empty() const;


	~LinkedList();

	class iterator {
	public:

		iterator();

		iterator& operator=(const iterator& rhs);
		const T& operator*() const;
		const T* operator->() const;
		iterator& operator++();		//prefix ++ operator
		iterator& operator++(int unused);  //postfix ++ operator; we dont care about this "unused" param
		bool operator==(const iterator& rhs) const;
		bool operator!=(const iterator& rhs) const;

		~iterator();
	private:
		
		iterator(typename LinkedList<T>::Node* paramCurr);
		typename LinkedList<T>::Node * current;

		friend iterator LinkedList<T>::begin() const;
		
	};

	iterator begin() const;
	iterator end() const;

private:
	struct Node
	{
		Node();
		T data;
		Node* next;
	};
	Node* head;
	int size;
};

template <typename T>
LinkedList<T>::iterator::iterator() : current(nullptr) {
}

template <typename T>
LinkedList<T>::iterator::iterator(Node* paramCurr) : current(paramCurr) {
}

template <typename T>
LinkedList<T>::iterator::~iterator() {
	current = nullptr;//no need to delete data here as the iterator class is only a subclass -> it operates on linked lists's data
}

template <typename T>
typename LinkedList<T>::iterator& LinkedList<T>::iterator::operator=(const iterator& rhs) {
	current = rhs.current;
	return *this;// support chaining
}

template <typename T>
const T& LinkedList<T>::iterator::operator*() const {
	return current->data;
}

template <typename T>
const T* LinkedList<T>::iterator::operator->() const {
	return &(operator*());
}

template <typename T>
typename LinkedList<T>::iterator& LinkedList<T>::iterator::operator++() {//prefix
	current = current->next;
	return *this;
}

template <typename T>
typename LinkedList<T>::iterator& LinkedList<T>::iterator::operator++(int unused) {//postfix
	return operator++();
}

template <typename T>
bool LinkedList<T>::iterator::operator==(const iterator& rhs) const {
	return current == rhs.current;
}

template <typename T>
bool LinkedList<T>::iterator::operator!=(const iterator& rhs) const {
	return !(this->operator==(rhs));
}

template<typename T>
LinkedList<T>::Node::Node() : next(nullptr) {

}

template<typename T>
LinkedList<T>::LinkedList() : head(nullptr), size(0)
{
}

template <typename T>
void LinkedList<T>::InsertFront(const T& data) {

	Node* tmp = new Node();
	tmp->data = data;

	tmp->next = head; //even if head is null, this is still correct
	head = tmp;

	size++;
}

template<typename T>
void LinkedList<T>::Insert(int index, const T& data) {

	if (index >= 0 && index < size) {

		Node* tmp = new Node();
		tmp->data = data;

		if (head) {
			if (index == 0) {
				tmp->next = head;
				head = tmp;
			}
			else {
				Node* prev = head;

				for (int i = 0; i < index - 1; i++) {
					prev = prev->next;
				}
				tmp->next = prev->next;
				prev->next = tmp;
			}
		}
		else {
			head = tmp;
		}
		size++;
	}
	else {
		if (!head) {// this is convenient since we can use only the Insert function in a loop, no need to work around function names

			Node* tmp = new Node();
			tmp->data = data;
			head = tmp;
			size++;
		}
		else {
			std::cout << "Insertion at index " << index << " is not possible as it does not exist!" << std::endl;
		}
	}
}

template <typename T>
void LinkedList<T>::InsertEnd(const T& data) {

	Node* tmp = new Node();
	tmp->data = data; //a new node has its next pointer set to null by default;

	Node* current = head;
	if (current) {
		for (int i = 0; i < size - 1; i++) {
			current = current->next;
		}
		current->next = tmp;
	}
	else head = tmp; // of head is null -> set it to temp

	size++;
}

template<typename T>
void LinkedList<T>::Delete(int index) {
	if (index >= 0 && index < size) {

		Node* current = head;
		Node* prev = nullptr;
		for (int i = 0; i < index; i++) {
			prev = current;
			current = current->next;
		}
		Node* next = current->next;
		if (prev) {
			prev->next = next;
		}
		else {
			head = next;// this is the case where we delete the head, so we assign the next node for a head
		}
		delete current;
		size--;
	}
	else {
		std::cout << "Index " << index << " is out of bounds!" << std::endl;
	}
}

template<typename T>
bool LinkedList<T>::Delete(std::function <bool (T&)> f) { //bool(*f)(const T& item)
		
		bool found = false;
		
		Node* current = head;
		Node* prev = nullptr;

		for (int i = 0; i < size; i++) {
			if (f(current->data)) {//current->data == value
				found = true;
				break;
			}
			prev = current;
			current = current->next;
		}
		if (found) {
			Node* next = current->next;
			if (prev) {
				prev->next = next;
			}
			else {
				head = next;// this is the case where we delete the head, so we assign the next node for a head
			}
			delete current;
			size--;
		}
		return found;
}

template <typename T>
int LinkedList<T>::Search(std::function <bool (T&)> f) const {
    
    Node* current = head;
    for (int i = 0; i < size; i++) {
        if (f(current->data)) {
            return i;
        }
        current = current->next;
    }

    return -1;// default return value to indicate the search value is not found
}

template<typename T>
void LinkedList<T>::Print() const {
	std::cout << "The linked list content is: ";
	if (!head) {
		std::cout << " <empty>." << std::endl;
		return;
	}
	Node* current = head;
	while (current) {
        std::cout << current->data;
        if (current->next) std::cout << ", ";
		current = current->next;
	}
    std::cout << std::endl;
}

template <typename T>
bool LinkedList<T>::Empty() const {
	return size;
}

template <typename T>
typename LinkedList<T>::iterator LinkedList<T>::begin() const {
	return iterator(head);
}

template <typename T>
typename LinkedList<T>::iterator LinkedList<T>::end() const {

	return iterator(); // in a linked list the end iteratiro is always null; the comarer compares them by data not identity
}

template<typename T>
LinkedList<T>::~LinkedList() {
	//delete all the alocated nodes + head
}

#endif // ! LinkedList_H
