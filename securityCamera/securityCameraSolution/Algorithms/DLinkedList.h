#ifndef DLinkedList_H
#define DLinkedList_H

#include <iostream>
#include <string>

// Doubly Linked List
//TODO: assignment, copying and deallocation (deletion)
template<typename T>
class DLinkedList {
public:
    DLinkedList();

    void InsertFront(const T& data);
    void Insert(int index, const T& data);
    void InsertEnd(const T& data);

    void Delete(int index);

    void Print() const;

    ~DLinkedList();
private:
    struct Node {
        Node();

        T data;
        Node* prev;
        Node* next;

        ~Node();
    };

    Node* head;
    int size;
};

template<typename T>
DLinkedList<T>::DLinkedList::Node::Node() : prev(nullptr), next(nullptr) {

}

template<typename T>
DLinkedList<T>::DLinkedList::Node::~Node() {

}

template<typename T>
DLinkedList<T>::DLinkedList() : head(nullptr), size(0) {

}

template<typename T>
void DLinkedList<T>::InsertFront(const T& data) {

    Node* tmp = new Node();
    tmp->data = data;

    if (head) {
        tmp->next = head;
        head->prev = tmp;
        head = tmp;
    }
    else {
        head = tmp;
    }
    size++;
}

template<typename T>
void DLinkedList<T>::Insert(int index, const T& data) {
    if (index >= 0 && index < size) {

        Node* tmp = new Node();
        tmp->data = data;

        Node* current = head;
        for (int i = 0; i < index; i++) {
            current = current->next;
        }
        tmp->next = current;
        if (current->prev) {
            current->prev->next = tmp;
            tmp->prev = current->prev;
        }
        current->prev = tmp;

        if (index == 0) {
            head = tmp;
        }
        size++;
    }
    else {
        if (head) {
            std::cout << "Insertion at index " << index << " is not possible as it does not exist!" << std::endl;
        }
        else {
            Node* tmp = new Node();
            tmp->data = data;
            head = tmp;
            size++;
        }
    }
}

template<typename T>
void DLinkedList<T>::InsertEnd(const T& data) {

    Node* tmp = new Node();
    tmp->data = data;

    if (head) {
        Node* current = head;
        while (current->next) {
            current = current->next;
        }
        current->next = tmp;
        tmp->prev = current;
    }
    else {
        head = tmp;
    }
    size++;
}

template<typename T>
void DLinkedList<T>::Delete(int index) {

    if (index >= 0 && index < size) {

        if (index == 0) {
            head = head->next;
            if (head) {
                delete head->prev;
                head->prev = nullptr;
                size--;
            }
            return;
        }

        Node* current = head;
        for (int i = 0; i < index; i++) {
            current = current->next;
        }
        current->prev->next = current->next;
        if (current->next) {
            current->next->prev = current->prev;
        }
        delete current;
        size--;
    }
    else {
        std::cout << "Index " << index << " is out of bounds!" << std::endl;
    }
}

template<typename T>
void DLinkedList<T>::Print() const {
    std::cout << "The doubly linked list content is: ";
    if (head) {
        Node* current = head;
        while (current) {
            std::cout << current->data;
            if (current->next) std::cout << ", ";
            current = current->next;
        }
    }
    else {
        std::cout << " <empty>." << std::endl;
    }
    std::cout << std::endl;
}

template<typename T>
DLinkedList<T>::~DLinkedList() {

}

#endif // !DLinkedList_H