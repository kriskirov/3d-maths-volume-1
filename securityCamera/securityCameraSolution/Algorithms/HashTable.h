#ifndef HashTable_H
#define HashTable_H

#include "LinkedList.h"
#include <string>

template <typename KEY, typename VAL>
class HashTable {
public:

	HashTable(); // default to 100 buckets
	HashTable(const unsigned int paramSize);// indices and sizes must be formalized, right now they are somehow different -> may lead to issue

	void AddItem(const KEY& key, const VAL& value);
	void RemoveItem(const KEY& key);
	void SearchKey(const KEY& key) const;
	void Print() const;

	~HashTable();

private:
	struct Bucket {

		Bucket();

		LinkedList<std::pair<KEY, VAL>> list;//default constructed
        // we could have stored the key here an NOT in the list: this would not require comparer/ lambda/ functror injection
        // still, this gives us actual privillages as we ca nsearch for whatever we may need and have the freedom
		~Bucket();
	};

	const unsigned int size; // this implementations is for fixed-size hash tables
    unsigned int count;
    Bucket* bucket;// an array of buckets

	size_t Hash(const KEY& key) const;
};


template<typename KEY, typename VAL>
HashTable<KEY, VAL>::Bucket::Bucket() {

}

template<typename KEY, typename VAL>
HashTable<KEY, VAL>::Bucket::~Bucket() {

}

template <typename KEY, typename VAL>
HashTable<KEY, VAL>::HashTable() : size(100), count(0) bucket(new Bucket[100]) {

}

template <typename KEY, typename VAL>
HashTable<KEY, VAL>::HashTable(const unsigned int paramSize) : size(paramSize), count(0), bucket(new Bucket[paramSize]) {

}

template <typename KEY, typename VAL>
HashTable<KEY, VAL>::~HashTable() {

}

template <typename KEY, typename VAL>
size_t HashTable<KEY, VAL>::Hash(const KEY& key) const {

	return std::hash<KEY>{}(key);
}

template <typename KEY, typename VAL>
void HashTable<KEY, VAL>::AddItem(const KEY& key, const VAL& value) {

	size_t hash = Hash(key);
	size_t index = hash % size;

	bucket[index].list.InsertEnd(std::pair<KEY, VAL>(key, value));
    count++;
}

template <typename KEY, typename VAL>
void HashTable<KEY, VAL>::RemoveItem(const KEY& key) {
	
    size_t index = Hash(key) % size;
	
    bool success = 
		bucket[index].list.Delete
	([&key](std::pair<KEY, VAL>& data) ->bool 
	{
		return data.first == key; 
	});
	
	if (success) {
        count--;
		std::cout << "Item '" << key << "' was removed from the hash table." << std::endl;
	}
	else {
		std::cout << "Item '" << key << "' was NOT removed from the hash table as it was NOT found." << std::endl;
	}
}

template <typename KEY, typename VAL>
void HashTable<KEY, VAL>::SearchKey(const KEY& key) const {
    
    size_t bucketIndex = Hash(key) % size;
    
    int listIndex =
        bucket[bucketIndex].list.Search
        ([&key](std::pair<KEY, VAL>& data) ->bool
    {
        return data.first == key;
    });

    if (listIndex != -1) {
        std::cout << "Item '" << key << "' was found at bucket " << bucketIndex << ", sub-index " << listIndex << "." << std::endl;
    }
    else {
        std::cout << "Item '" << key << "' was NOT found!" << std::endl;
    }
}

template <typename KEY, typename VAL>
void HashTable<KEY, VAL>::Print() const {

    if (count) {
	    
        typedef LinkedList<std::pair<KEY, VAL>>::iterator iter;
        
        for (unsigned int i = 0; i < size; i++) {
            std::cout << "Printing -> Hash table bucket index " << i << " contains:\n";
            for (iter k = bucket[i].list.begin(); k != bucket[i].list.end(); k++) {
                std::cout << k->first << ": " << k->second << std::endl;
            }
            std::cout << std::endl;
        }
    }
    else {
        std::cout << "Printing -> Hash table is <empty>." << std::endl;
    }
}
#endif // !HashTable_H