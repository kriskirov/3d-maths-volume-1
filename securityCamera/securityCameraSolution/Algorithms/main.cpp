#include <iostream>
#include "Algorithms.h"
#include "HashTable.h"
#include "LinkedList.h"
#include <stdlib.h>
#include <time.h>
#include <string>

int main()
{

    srand(time(0));
    
    char anyKey;
        {
            std::cout << std::endl;

            std::cout << "========================================================================\n";
            std::cout << "========================== Quick Sort and Binary Search ================\n";
            std::cout << "========================================================================\n" << std::endl;

            
            int someArray[] = { 55,4,8,90,13,77,1,98,64,1000 };
            int sizeOfArray = sizeof(someArray) / sizeof(someArray[0]);


            std::cout << "This array is filled with random values: ";
            for (int i = 0; i < sizeOfArray; i++) {
                std::cout << someArray[i];
                if (i != sizeOfArray - 1) std::cout << ", ";
            }

            quickSort(someArray, 0, sizeOfArray - 1);
            
            std::cout << std::endl;
            std::cout << "The array is Quick sorted and consists of: ";
            for (int i = 0; i < sizeOfArray; i++) {
                std::cout << someArray[i];
                if (i != sizeOfArray - 1) std::cout << ", ";
            }

            std::cout << std::endl;
            std::cout << "Input a search value: ";
            int userInput;
            std::cin >> userInput;

            int resultFirstMatch = binarySearch<int>(someArray, sizeOfArray, userInput);
            if (resultFirstMatch != -1) {
                std::cout << "The value " << userInput << " was found at index " << resultFirstMatch
                    << " (first match)." << std::endl;
            }
            else {
                std::cout << "The value " << userInput << " was not found! (first match)" << std::endl;
            }

            int resultFirstOcc = binarySearch<int>(someArray, sizeOfArray, userInput, false);
            if (resultFirstOcc != -1) {
                std::cout << "The value " << userInput << " was found at index " << resultFirstOcc
                    << " (first occurance)." << std::endl;
            }
            else {
                std::cout << "The value " << userInput << " was not found! (first occurance)" << std::endl;
            }

            int resultLastOcc = binarySearch<int>(someArray, sizeOfArray, userInput, false, false);
            if (resultLastOcc != -1) {
                std::cout << "The value " << userInput << " was found at index " << resultLastOcc
                    << " (last occurance)." << std::endl;
            }
            else {
                std::cout << "The value " << userInput << " was not found! (last occurance)" << std::endl;
            }
        }

        std::cout << "[Enter] any key to continue..." << std::endl;
        std::cin >> anyKey;

		//BST
		{
            std::cout << std::endl;

            std::cout << "========================================================================\n";
            std::cout << "========================== Binary Search Tree ==========================\n";
            std::cout << "========================================================================\n" << std::endl;

			BSTree<int> tree;

			int i[] = { 55,30,8,0,5 };
			std::cout << std::endl << "We now insert values into a binary search tree...\n" << std::endl;

			tree.Insert(i[0]);
			tree.Insert(i[1]);
			tree.Insert(i[2]);
            tree.Insert(i[3]);
            tree.Insert(i[4]);
            tree.TraverseInorder();
			
            std::cout << "Input a search value: " << std::endl;
			int searchValue;
			std::cin >> searchValue;

			if (tree.Search(searchValue)) {
				std::cout << "Search value " << searchValue << " is FOUND!" << std::endl;
			}
			else {
				std::cout << "Search value " << searchValue << " is NOT found." << std::endl;
			}

			std::cout << "Traversing tree in Preorder...\n" << std::endl;
			tree.TraversePreorder(); 
			
            std::cout << "Traversing tree in Inorder...\n" << std::endl;
			tree.TraverseInorder(); 
			
            std::cout << "Traversing tree in Postorder...\n" << std::endl;
			tree.TraversePostorder(); 

            std::cout << "[Enter] any key to continue..." << std::endl;
            std::cin >> anyKey;

            std::cout << std::endl << "We now delete all the values: " << i[0] << ", " << i[2] << ", " << i[4] << " from the binary search tree...\n" << std::endl;
            std::cout << std::endl << "Deleting: " << i[0] << "\n" << std::endl;
            tree.Delete(i[0]);
			tree.TraverseInorder();

            std::cout << std::endl << "Deleting: " << i[2] << "\n" << std::endl;
            tree.Delete(i[2]);
			tree.TraverseInorder();

            std::cout << std::endl << "Deleting: " << i[4] << "\n" << std::endl;
			tree.Delete(i[4]);
			tree.TraverseInorder();
			
			
		}

        std::cout << "[Enter] any key to continue..." << std::endl;
        std::cin >> anyKey;

        //Linked List
        {
            std::cout << std::endl;

            std::cout << "========================================================================\n";
            std::cout << "========================== Linked List =================================\n";
            std::cout << "========================================================================\n" << std::endl;

            LinkedList<int> someLinkedList;
            int i[] = {4,31,0,44,9};


            someLinkedList.Insert(0,i[0]);//list is 4
            someLinkedList.Print();
            someLinkedList.Insert(0, i[1]);//list is 31,4
            someLinkedList.Print();
            someLinkedList.Insert(1, i[2]);//list is 31,0,4
            someLinkedList.Print();
            someLinkedList.Insert(0, i[3]);//list is 44,31,0,4
            someLinkedList.Print();
            
            std::cout << "Input a search value: " << std::endl;

            int result = someLinkedList.Search(
                [&i] (int &data)->bool {
                return data == i[0];
            });

            if (result == -1) {
                std::cout << "Value " << i[0] << " was not found in the linked list!" << std::endl;
            }
            else {
                std::cout << "Value " << i[0] << " was found in index " << result << " of the linked list!" << std::endl;
            }
            
            std::cout << "Deleting values from the linked list at various indices: " << std::endl;
            someLinkedList.Delete(0); // list is 31,0,4
            someLinkedList.Print();
            someLinkedList.Delete(1); // list is 31,4
            someLinkedList.Print();
            someLinkedList.Delete(1); // list is 31
            someLinkedList.Print();
            someLinkedList.Delete(0); // list is <empty>
            someLinkedList.Print();
        }

        std::cout << "[Enter] any key to continue..." << std::endl;
        std::cin >> anyKey;

        //Doubly Linked List
        {
            std::cout << std::endl;

            std::cout << "========================================================================\n";
            std::cout << "========================== Doubly Linked List ==========================\n";
            std::cout << "========================================================================\n" << std::endl;

            DLinkedList<int> someDLinkedList;
            int i[] = { 8,3,0,1,2 };
            
            someDLinkedList.Insert(0,i[0]); // list is 8
            someDLinkedList.Print();
            someDLinkedList.Insert(0, i[1]); // list is 3,8
            someDLinkedList.Print();
            someDLinkedList.Insert(1, i[2]); // list is 3,0,8
            someDLinkedList.Print();
            someDLinkedList.Insert(1, i[3]); // list is 3,1,0,8
            someDLinkedList.Print();
            someDLinkedList.Delete(3); // list is 3,1,0
            someDLinkedList.Print();
            someDLinkedList.Delete(0); // list is 1,0
            someDLinkedList.Print();
            someDLinkedList.Delete(0); // list is 0
            someDLinkedList.Print();
            someDLinkedList.Delete(0); // list is 0
            someDLinkedList.Print(); // list is <empty>

        }
		
        std::cout << "[Enter] any key to continue..." << std::endl;
        std::cin >> anyKey;

		// Hash table
		{
			std::cout << std::endl;

            std::cout << "========================================================================\n";
            std::cout << "========================== Hash Table ==================================\n";
            std::cout << "========================================================================\n" << std::endl;
            HashTable<std::string, std::string> hashTable = HashTable<std::string, std::string>(5);
            
            hashTable.Print();

			std::cout << "\nCreating a hash table of type <inventory item name, description>:\n" << std::endl;

			hashTable.AddItem("Rusty Sword","A sword covered in rust.");
			hashTable.AddItem("Helmet","A shiny piece of craftsmenship! Exotic!");
			hashTable.AddItem("Bastard Sword","This sword type is only used by the mightiest warriors.");
			hashTable.AddItem("Guard Armor", "The royal coat of arms is embroided on the leather.");
			hashTable.AddItem("Letter from the Fire mages", "A strange seal encloses the paper piece.");
			hashTable.AddItem("Magic Ring", "It pulses with arcane energy.");

			hashTable.Print();
           
            std::cout << "[Enter] any key to continue..." << std::endl;
            std::cin >> anyKey;
			
			std::cout << "==========================================" << std::endl;
			hashTable.RemoveItem("Rusty Sword");
			hashTable.RemoveItem("Guard Armor");
			hashTable.RemoveItem("Asd");
			std::cout << "==========================================" << std::endl;

			hashTable.Print();

            hashTable.SearchKey("Letter from the Fire mages");
		}

        std::cout << "[Enter] any key to continue..." << std::endl;
        std::cin >> anyKey;
        //create destructors for data structures so we can avoid memory leaks

    return 0;
}