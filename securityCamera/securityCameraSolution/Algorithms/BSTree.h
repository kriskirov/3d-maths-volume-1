#ifndef BSTree_H
#define BSTree_H

#include <iostream>
#include <string>

//Binary Search Tree
//TODO implement cypying, assignmnt and deletion of the structure, it is leaking now

template<typename T>
class BSTree {
public:

    BSTree();
    ~BSTree();

    // Interface functions that use the recursive ones below.
    // I know this looks unclean but it is only for the sake of demonstrating recursive functionality.
    // All this non-sense is so that the pointer does not end
    // up to the user of the class interface
    void Insert(const T& data);

    bool Search(const T& data) const;
    void TraversePreorder() const;
    void TraverseInorder() const;
    void TraversePostorder() const;

    void Delete(const T& data);

private:
    struct Node {

        Node();

        T data;
        Node* left;
        Node* right;

        ~Node();
    };

    Node* root;

    unsigned int size;

    //functions that work recursively
    Node* Insert(Node* root, const T& data);

    bool Search(Node* root, const T& data) const;
    void TraversePreorder(Node* root) const;
    void TraverseInorder(Node* root) const; // we should not modify the tree directly
    void TraversePostorder(Node* root) const;
    Node* FindMin(Node* root) const;

    Node* Delete(Node* root, const T& data);
};

template <typename T>
void BSTree<T>::Insert(const T& data) {
    root = Insert(root, data);
}

template <typename T>
bool BSTree<T>::Search(const T& data) const {
    return Search(root, data);
}

template <typename T>
void BSTree<T>::TraversePreorder() const {
    std::cout << "The Preorder traversal of the BST is: ";
    TraversePreorder(root);
    std::cout << std::endl;
}

template <typename T>
void BSTree<T>::TraverseInorder() const {
    std::cout << "The Inorder traversal of the BST is: ";
    TraverseInorder(root);
    std::cout << std::endl;
}

template <typename T>
void BSTree<T>::TraversePostorder() const {
    std::cout << "The Postorder traversal of the BST is: ";
    TraversePostorder(root);
    std::cout << std::endl;
}

template <typename T>
void BSTree<T>::Delete(const T& data) {
    root = Delete(root, data);
}

template <typename T>
BSTree<T>::Node::Node() : left(nullptr), right(nullptr) {// thisneeds to become a constructor without params

}

template <typename T>
BSTree<T>::Node::~Node() {

}


template <typename T>
BSTree<T>::BSTree() : root(nullptr), size(0) {

}

template <typename T>
BSTree<T>::~BSTree() {

}

template <typename T>
typename BSTree<T>::Node* BSTree<T>::Insert(Node* root, const T& data) {
    if (root == nullptr) {
        root = new Node();
        root->data = data;
        std::cout << "Node " << data << " inserted successfully!" << std::endl;
    }
    else if (data <= root->data) {
        root->left = Insert(root->left, data);
    }
    else {
        root->right = Insert(root->right, data);
    }
    return root;
}

template <typename T>
bool BSTree<T>::Search(Node* root, const T& data) const {
    if (root == nullptr) return false;
    else if (data == root->data) return true;
    else if (data <= root->data) return Search(root->left, data);
    else return Search(root->right, data);
}

template <typename T>
void BSTree<T>::TraversePreorder(Node* root) const { // we can have functors/predicates as a parameter. 
                                                     //(caution must be applied) when modifying a value (a tree recalculation is potentially needed)
    if (root == nullptr) return;
    // we use printing for proof of concept only
    std::cout << root->data << " ";
    TraversePreorder(root->left);
    TraversePreorder(root->right);
}

template <typename T>
void BSTree<T>::TraverseInorder(Node* root) const {
    //in this implementation, this traversal gives us he content of the tree in ascending order of data
    if (root == nullptr) return;

    TraverseInorder(root->left);
    std::cout << root->data << " ";
    TraverseInorder(root->right);
}

template <typename T>
void BSTree<T>::TraversePostorder(Node* root) const {

    if (root == nullptr) return;

    TraversePostorder(root->right);
    TraversePostorder(root->left);
    std::cout << root->data << " ";
}

template <typename T>
typename BSTree<T>::Node* BSTree<T>::FindMin(Node* root) const {
    if (root == nullptr) return root;
    else if (root->left == nullptr) {
        return root;
    }
    else {
        return FindMin(root->left);
    }
}

template <typename T>
typename BSTree<T>::Node* BSTree<T>::Delete(Node* root, const T& data) {
    if (root == nullptr) {
        std::cout << "Node " << data << "could not be deleted as it was not found!" << std::endl;
        return root;
    }
    else if (data < root->data) root->left = Delete(root->left, data);
    else if (data > root->data) root->right = Delete(root->right, data);
    else {
        if (root->left == nullptr && root->right == nullptr) {
            delete root;
            root = nullptr;
            std::cout << "Node " << data << " deleted successfully!" << std::endl;
        }
        else if (root->left == nullptr) {
            Node* tmp = root;
            root = root->right;
            delete tmp;
            std::cout << "Node " << data << " deleted successfully!" << std::endl;
        }
        else if (root->right == nullptr) {
            Node* tmp = root;
            root = root->left;
            delete tmp;
            std::cout << "Node " << data << " deleted successfully!" << std::endl;
        }
        else {
            Node* tmp = FindMin(root->right);
            std::cout << "Replacing node " << root->data << " with" << tmp->data << "!" << std::endl;
            root->data = tmp->data;
            root->right = Delete(root->right, tmp->data);
        }
    }
    return root;
}
#endif // !BSTree_H