#include "Vector3.h"
#include "MathUtility.h"
#include <math.h>
#include "Format.h"
#include <iostream>

Vector3::Vector3()
{
  
}

Vector3::Vector3(float pX, float pY, float pZ) : x(pX), y(pY), z(pZ)
{
}

Vector3::~Vector3()
{
}

float Vector3::Angle(const Vector3& lhs, const Vector3& rhs, const Format::Angle angleFormat)// there must be an option to choose the return value format - radians or degrees
// use only one measure and then create a macro that converts to degrees/radians - G.Popov
{
    float angleInRads = acos(lhs.Dot(rhs) / (lhs.Length()*rhs.Length()));

    if (angleFormat == Format::Angle::Degrees) return (angleInRads / (PI()*2.0f))*360.0f;
    
    return angleInRads;
}

bool Vector3::operator==(const Vector3 & rhs) const
{
	return (x == rhs.x && y == rhs.y && z == rhs.z);
}

bool Vector3::operator!=(const Vector3 & rhs) const
{
	return !(*this==rhs);
}

Vector3 Vector3::Normalized() const
{
    float lengthOfThis = this->Length();
    float factor = 1.0f / lengthOfThis;
	//float factor = 0;
    //if (lengthOfThis != 0) {
      //   factor = 1.0f / lengthOfThis;
    //}
    return Vector3(x * factor, y * factor, z * factor);
}

const Vector3 Vector3::Up = Vector3(0, 0, 1);
const Vector3 Vector3::Forward = Vector3(1, 0, 0);
const Vector3 Vector3::Left = Vector3(0, 1, 0);
const Vector3 Vector3::Zero = Vector3(0, 0, 0);
