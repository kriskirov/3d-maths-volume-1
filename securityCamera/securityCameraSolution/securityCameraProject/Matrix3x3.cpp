#include "Matrix3x3.h"
#include "Vector3.h"
#include <math.h>
#include <iostream>
#include "MathUtility.h"


//by default, an identity matrix is constructed
Matrix3x3::Matrix3x3() : col1(Vector3(1, 0, 0)), col2(Vector3(0, 1, 0)), col3(Vector3(0, 0, 1))
{
}

Matrix3x3::Matrix3x3(const Vector3 & p_col1, const Vector3 & p_col2, const Vector3 & p_col3) :
    col1(p_col1),
    col2(p_col2),
    col3(p_col3)
{

}


Matrix3x3::~Matrix3x3()
{
}

Matrix3x3 Matrix3x3::Identity()
{
    return Matrix3x3();
}

Matrix3x3 Matrix3x3::XRotationMatrix(const Vector3& from, const Vector3& to) {
    
    Matrix3x3 returnMat;
    if (from != Vector3::Zero 
        && to != Vector3::Zero
        && Vector3(0, from.y, from.z) != Vector3::Zero
        && Vector3(0, to.y, to.z) != Vector3::Zero) {// we can only talk about rotation of we have an actual angle -> zero vectors are of no use

        Vector3 normFrom = from.Normalized();
        Vector3 normTo = to.Normalized();

        // if the to vector lies in a perfectly opposite direction -> use ccw rotation as default
        // we use the 2D determinant to evaluate direction of rotation -> cw or ccw
        float xAmountAngleSign = (Sign(normFrom.y*normTo.z - normTo.y*normFrom.z) < 0.0f) ? -1 : 1;
        //we get the needed angle and give it a sign for its proper direction of rotation
        float xAmount = xAmountAngleSign *
            Vector3::Angle(Vector3(0, normFrom.y, normFrom.z).Normalized(), Vector3(0, normTo.y, normTo.z).Normalized(), Format::Angle::Radians);
        //we construct a rotation matrix for the given plane (axis) by the right hand rule standard 
        //(we could use custom rule based on a custom choice of axis directions, etc.)
        Matrix3x3 xAxisRotMatrix(Vector3(1, 0, 0), Vector3(0, cos(xAmount), sin(xAmount)), Vector3(0, -sin(xAmount), cos(xAmount)));
        returnMat = xAxisRotMatrix * returnMat;
        std::cout << "xRot" << std::string(xAxisRotMatrix) << std::endl;
    }
    return returnMat;
}

Matrix3x3 Matrix3x3::YRotationMatrix(const Vector3& from, const Vector3& to) {
    
    Matrix3x3 returnMat;
    if (from != Vector3::Zero 
        && to != Vector3::Zero
        && Vector3(from.x, 0, from.z) != Vector3::Zero
        && Vector3(to.x, 0, to.z) != Vector3::Zero) {

        Vector3 normFrom = from.Normalized();
        Vector3 normTo = to.Normalized();

        float yAmountAngleSign = (Sign(normFrom.x*normTo.z - normTo.x*normFrom.z) < 0.0f) ? 1 : -1;
        float yAmount = yAmountAngleSign *
            Vector3::Angle(Vector3(normFrom.x, 0, normFrom.z).Normalized(), Vector3(normTo.x, 0, normTo.z).Normalized(), Format::Angle::Radians);
        Matrix3x3 yAxisRotMatrix(Vector3(cos(yAmount), 0, -sin(yAmount)), Vector3(0, 1, 0), Vector3(sin(yAmount), 0, cos(yAmount)));
        returnMat = yAxisRotMatrix * returnMat;
        std::cout << "yRot" << std::string(yAxisRotMatrix) << std::endl;
    }
    return returnMat;
}

Matrix3x3 Matrix3x3::ZRotationMatrix(const Vector3& from, const Vector3& to) {
    
    Matrix3x3 returnMat;
    if (from != Vector3::Zero 
        && to != Vector3::Zero
        && Vector3(from.x, from.y, 0) != Vector3::Zero
        && Vector3(to.x, to.y, 0) != Vector3::Zero) {

        Vector3 normFrom = from.Normalized();
        Vector3 normTo = to.Normalized();

        float zAmountAngleSign = (Sign(normFrom.x*normTo.y - normTo.x*normFrom.y) < 0.0f) ? 1 : -1;
        float zAmount = zAmountAngleSign *
            Vector3::Angle(Vector3(normFrom.x, normFrom.y, 0).Normalized(), Vector3(normTo.x, normTo.y, 0).Normalized(), Format::Angle::Radians);
        Matrix3x3 zAxisRotMatrix(Vector3(cos(zAmount), sin(zAmount), 0), Vector3(-sin(zAmount), cos(zAmount), 0), Vector3(0, 0, 1));
        returnMat = zAxisRotMatrix*returnMat;
        std::cout << "zRot" << std::string(zAxisRotMatrix) << std::endl;
    }
    return returnMat;
}

Matrix3x3 Matrix3x3::LookAtRotationMatrix(const Vector3& from, const Vector3& to) //from and to are directions, not positions 
{    
    //we construct rotation matrices based on direction differences
    //we use the right handed coordinate system standard during matrices construction
    Matrix3x3 returnMat;//is identity by default
    if (from != Vector3::Zero && to != Vector3::Zero) {// we can only talk about rotation of we have an actual angle -> zero vectors are of no use

        Vector3 normFrom = from.Normalized();
        Vector3 normTo = to.Normalized();

        //originally test if we have the case where we have to rearrange the multiplications (a *from* direction lies on one of the basis)
        bool fromLiesOnXBasis = (normFrom == Vector3::Forward) ? true : false;// we understand world space
        bool fromLiesOnYBasis = (normFrom == Vector3::Left) ? true : false;
        //bool fromLiesOnZBasis = (normFrom == Vector3::Up) ? true : false; // this case is the default case any way
        //depending on the "from" direction there exist 3 cases of matrix construction order
        if (fromLiesOnXBasis) 
        {
            Matrix3x3 yRot = Matrix3x3::YRotationMatrix(normFrom, normTo);
            normFrom = yRot * normFrom;
            Matrix3x3 zRot = Matrix3x3::ZRotationMatrix(normFrom, normTo);
            normFrom = zRot * normFrom;
            Matrix3x3 xRot = Matrix3x3::XRotationMatrix(normFrom, normTo);
            returnMat = xRot * zRot * yRot * returnMat;
            return returnMat;
        }
        else if (fromLiesOnYBasis) 
        {
            Matrix3x3 zRot = Matrix3x3::ZRotationMatrix(normFrom, normTo);
            normFrom = zRot * normFrom;
            Matrix3x3 xRot = Matrix3x3::XRotationMatrix(normFrom, normTo);
            normFrom = xRot * normFrom;
            Matrix3x3 yRot = Matrix3x3::YRotationMatrix(normFrom, normTo);
            returnMat = yRot * xRot * zRot * returnMat;
            return returnMat;
        }
        else 
        {
            Matrix3x3 xRot = Matrix3x3::XRotationMatrix(normFrom, normTo);
            normFrom = xRot * normFrom;
            Matrix3x3 yRot = Matrix3x3::YRotationMatrix(normFrom, normTo);
            normFrom = yRot * normFrom;
            Matrix3x3 zRot = Matrix3x3::ZRotationMatrix(normFrom, normTo);
            returnMat = zRot * yRot * xRot * returnMat;
            return returnMat;
        }
    }
    return returnMat;
}

Vector3 Matrix3x3::operator*(const Vector3& rhs) const
{
    //return Vector3(col1.x*rhs.x + col2.x*rhs.y + col3.x*rhs.z, col1.y*rhs.x + col2.y*rhs.y + col3.y*rhs.z, col1.z*rhs.x + col2.z*rhs.y + col3.z*rhs.z);
    return Vector3(GetXRow().Dot(rhs), GetYRow().Dot(rhs), GetZRow().Dot(rhs));
}

Matrix3x3 Matrix3x3::operator*(const Matrix3x3& rhs) const
{
    //This is so stupid on my side as I could have just dotted the rows and columns if only i had implemented a way to get the columns AND the rows :D
    Vector3 xBasis
    (
        GetXRow().Dot(rhs.GetXBasis()),
        GetYRow().Dot(rhs.GetXBasis()),
        GetZRow().Dot(rhs.GetXBasis())
    );

    Vector3 yBasis
    (
        GetXRow().Dot(rhs.GetYBasis()),
        GetYRow().Dot(rhs.GetYBasis()),
        GetZRow().Dot(rhs.GetYBasis())
    );

    Vector3 zBasis
    (
        GetXRow().Dot(rhs.GetZBasis()),
        GetYRow().Dot(rhs.GetZBasis()),
        GetZRow().Dot(rhs.GetZBasis())
    );


    return Matrix3x3(xBasis,yBasis,zBasis);
}

Vector3 Matrix3x3::GetXBasis() const
{
    return Vector3(col1.x,col1.y,col1.z);
}

Vector3 Matrix3x3::GetYBasis() const
{
    return Vector3(col2.x,col2.y,col2.z);
}

Vector3 Matrix3x3::GetZBasis() const
{
    return Vector3(col3.x,col3.y,col3.z);
}

Vector3 Matrix3x3::GetXRow() const
{
    return Vector3(col1.x,col2.x,col3.x);
}

Vector3 Matrix3x3::GetYRow() const
{
    return Vector3(col1.y,col2.y,col3.y);
}

Vector3 Matrix3x3::GetZRow() const
{
    return Vector3(col1.z,col2.z,col3.z);
}

void Matrix3x3::LookAt(const Vector3& lookAtPos, const Vector3& upVec) {
    // case when lookAt and upVec match up is not handled
    // use quaternion rotation to adjust desired look at axis to be the facing axis
    //check the ordering of the Cross product - important!!!
    Vector3 newX(lookAtPos.Normalized());//the X basis of the returned matrix contains the axis that looks at the target (lookAt position)
    Vector3 newY(upVec.Normalized().Cross(newX).Normalized());
	Vector3 newZ(newX.Cross(newY));//No need to normalize as the cross product of  2 unit vectors returns a perpendicular vector with the length of 1
    col1 = newX;
    col2 = newY;
    col3 = newZ;
}
