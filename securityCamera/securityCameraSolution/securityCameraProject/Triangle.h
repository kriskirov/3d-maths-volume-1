#ifndef Triangle_H
#define Triangle_H

#include "Vector3.h"
#include <string>

class Triangle {
public:
    Triangle();
    Triangle(Vector3 &a, Vector3 &b, Vector3 &c);

    inline operator std::string() const {
        return std::string(pointA) + "---" + std::string(pointB) + "---" + std::string(pointC);
    }
    
    Vector3 GetPointA() const;
    Vector3 GetPointB() const;
    Vector3 GetPointC() const;
    Vector3 GetNormal() const;
    Vector3 Project(Vector3 &vec) const;

private:

    Vector3 CalculateNormal();

    Vector3 pointA, pointB, pointC;
    Vector3 normal;
    
};

#endif // !Triangle_H
