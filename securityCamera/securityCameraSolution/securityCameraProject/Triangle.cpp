#include "Triangle.h"

Triangle::Triangle() : 
    pointA(Vector3::Zero),
    pointB(Vector3::Zero),
    pointC(Vector3::Zero),
    normal(CalculateNormal())
{

}

Triangle::Triangle(Vector3 &a, Vector3 &b, Vector3 &c) : 
    pointA(a),
    pointB(b),
    pointC(c),
    normal (CalculateNormal())
{
   
}

Vector3 Triangle::GetPointA() const {
    return pointA;
}

Vector3 Triangle::GetPointB() const {
    return pointB;
}

Vector3 Triangle::GetPointC() const {
    return pointC;
}

Vector3 Triangle::GetNormal() const {
    return normal;
}

Vector3 Triangle::Project(Vector3 &offsetVec) const {
    
    float d = offsetVec.Dot(normal); // no need to divide by normal's length as it is 1 
    //we inverse the vec (-) as we are going to use it as an offset vec to get the back on the surface of the plane
    Vector3 correctionVec = normal*-d;
    return offsetVec + correctionVec;
}

//if winding order is CW -> the normal is positive Z (negative z otherwize)
Vector3 Triangle::CalculateNormal() {
    
    Vector3 vecOnPlane1 = pointA - pointB;
    Vector3 vecOnPlane2 = pointC - pointB;

    return (vecOnPlane1.Cross(vecOnPlane2)).Normalized();
}
