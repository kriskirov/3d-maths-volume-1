#ifndef Vector3_h
#define Vector3_h

#include <math.h>
#include <string>
#include "Format.h"

class Vector3
{
public:
    Vector3();
    Vector3(float x, float y, float z);
    ~Vector3();

    inline float Dot(const Vector3& rhs) const 
    { 
        return (x*rhs.x) + (y*rhs.y) + (z*rhs.z); 
    }
    
    //cross product for a right hand coordinate system. Make sure quaternions follow the trend if applicable.
    inline Vector3 Cross(const Vector3& rhs) const
    {
        Vector3 v;

        v.x = y*rhs.z - (z*rhs.y);
        v.y = z*rhs.x - (x*rhs.z);
        v.z = x*rhs.y - (y*rhs.x);

        return v;
    }

    inline float Length() const
    {
        return sqrt((x*x) + (y*y) + (z*z));
    }

    Vector3 Normalized() const;

    static float Angle(const Vector3& lhs, const Vector3& rhs, const Format::Angle angleFormat);

    inline Vector3 operator+(const Vector3& rhs) const
    {
        return Vector3(x + rhs.x, y + rhs.y, z + rhs.z);
    }

    inline Vector3 operator-(const Vector3& rhs) const
    {
        return Vector3(x - rhs.x, y - rhs.y, z - rhs.z);
    }

    inline Vector3 operator*(const float rhs) const
    {
        return Vector3(x*rhs, y*rhs, z*rhs);
    }

	bool operator==(const Vector3& rhs) const;

	bool operator!=(const Vector3& rhs) const;

    inline operator std::string() const {
        return "<" + std::to_string(x) + ", " + std::to_string(y) + ", " + std::to_string(z) + ">";
    }

    float x, y, z;

    static const Vector3 Up;
    static const Vector3 Forward;
    static const Vector3 Left;
	static const Vector3 Zero;
};

#endif // !Vector3_h