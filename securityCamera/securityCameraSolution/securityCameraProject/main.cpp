#include "Vector3.h"
#include "Quaternion.h"
#include "Matrix3x3.h"
#include "Triangle.h"
#include "Format.h"
#include <iostream>

int main() {
//Only quaternions and vectors are needed since we are only interested in rotation of a prop.
//No matrix needed as we need no scale nor movement.

    std::cout << "========= START OF DEBUG LINES =============" << std::endl;
    
    Vector3 point (1,0,0);
    std::cout << " \n point = " << std::string(point) << std::endl;
    Quaternion q (Vector3(0,0,1),90);
    std::cout << " \n q(half rotation quaternion) = " << std::string(q) << std::endl;
    Vector3 result = q*point;
    std::cout<<" \n q*point(vector part) = "<< std::string(result) << std::endl;

    std::cout << "\n\n Angle comparison... " << std::endl;
    Vector3 p1(1, 0, 0);
    Vector3 p2(0, -2, 0);
    std::cout << "\n The angle between p1 " << std::string(p1) << " and " << "p2 " << std::string(p2) << " is: " << Vector3::Angle(p1,p2,Format::Angle::Degrees) << " degrees" << std::endl;


    Matrix3x3 camTransform;
    Matrix3x3 rotatedCamTransform;
    std::cout << "\n\n LookAt test... " << std::endl;
    std::cout << "\n The Matrix before LookAt:\n" << std::string(rotatedCamTransform) << std::endl;
    Vector3 posVec(1,1,0);
    Vector3 upVec(Vector3::Up);// TODO: Implement copy constructor (or keep using default)
    std::cout << "\n LookAt target position is: " << std::string(posVec) << " and the LookAt up-axis is: " << std::string(upVec) << std::endl;
    rotatedCamTransform.LookAt(posVec, upVec);
    std::cout << "\n The matrix after LookAt transformation:\n" << std::string(rotatedCamTransform) << std::endl;

    std::cout << "\n\n Angle comparison..." << std::endl;
    std::cout << " The angle between the original X basis and the rotated X basis is " << Vector3::Angle(camTransform.col1, rotatedCamTransform.col1,Format::Angle::Degrees)
        << " degrees" << std::endl; //TODO: instead of col1, use colX - it is more intuitive
    std::cout << std::endl;

    std::cout << "========= END OF DEBUG LINES =============" << std::endl;

    std::cout << "\n================================= PLANES ======================================" << std::endl;
    {

        Triangle triangle(Vector3(0, 0, 1), Vector3(0, 1, 0), Vector3(1, 0, -1));
        Vector3 offset;
        std::cout << "A triangle is defined as:\n" << std::string(triangle) << "\nand normal " << std::string(triangle.GetNormal()) << std::endl;
        std::cout << "Input any vector for which you want to have the projection that lies on the plane defined by the triangle\n:";
        std::cout << "(absolute world coordinates)\n";
        
        std::cout << "X: ";
        std::cin >> offset.x;
        std::cout << "\n";
        std::cout << "Y: ";
        std::cin >> offset.y;
        std::cout << "\n";
        std::cout << "Z: ";
        std::cin >> offset.z;
        std::cout << "\n";

        Vector3 projection = triangle.Project(offset);
        std::cout << "The projected vector is: " << std::string(projection) << " (absolute world coordinates)\n";
        std::cout << "The dot product of the projected vector and the normal vector must be 0.\n"
            "Currently it is " << projection.Dot(triangle.GetNormal()) << std::endl;
    }
    
    std::cout << "\n========= ROTATION WITH LOOK AT BASIS RECONSTRUCTION =============\n" << std::endl;
    {
        Vector3 inputLookAt;
        Matrix3x3 unmodifiedMatrix;
        Matrix3x3 modifiedMatrix;
        std::cout << "Using right-handed coordinate system, creating a look at matrix by basis construction" << std::endl;
        std::cout << "Input the x, y and z coordinates of the Look At position" << std::endl;
        std::cout << "WARNING: The x basis of a matrix is considered the look at (forward) vector of the transform(matrix)" << std::endl;
        std::cout << "The matrix before Look At vertical rotation transformation is:\n" << std::string(modifiedMatrix) << std::endl;

        std::cout << "X: ";
        std::cin >> inputLookAt.x;
        std::cout << std::endl;

        std::cout << "Y: ";
        std::cin >> inputLookAt.y;
        std::cout << std::endl;

        std::cout << "Z: ";
        std::cin >> inputLookAt.z;
        std::cout << std::endl;

        float angle = Vector3::Angle(unmodifiedMatrix.col1, inputLookAt, Format::Angle::Degrees);// I take the unmodified identity matrix as an initial orientation
                                                                         // This orientation serves as a comparing matrix; I could use Vector3::Up
        std::cout << "The angle between the X basis and the input vector is " << angle << " degrees" << std::endl;

        modifiedMatrix.LookAt(inputLookAt, Vector3::Up);
        std::cout << "The matrix after Look At vertical rotation transformation is:\n" << std::string(modifiedMatrix) << std::endl;

        Vector3 arbitrary(0, 1, 0);
        Vector3 transformedVec = modifiedMatrix * arbitrary;
        std::cout << "This matrix can now be applied to an arbitrary vector like:" << std::string(arbitrary)
            << "\nto produce a transformed vector: " << std::string(transformedVec) <<std::endl;

    }
    std::cout << "\n========= ROTATION WITH ROTATION MATRIX CONSTRUCTION =============" << std::endl;

    std::cout << "=============================================================================\n";
    std::cout << "========================= Simulating security camera ========================\n";
    std::cout << "=============================================================================\n" << std::endl;
	while (true)
	{
        std::cout << "=============================================================================\n" << std::endl;

        Matrix3x3 unmodifiedMatrix;
        float angle;

		Vector3 fromDirection(1, 0, 0);//This will hold a direction from which a rotation transformation matrix will be constructed. This is the "start" of the transformation.
		//It does not need to be normalized
		Vector3 inputToDirection;//This will hold a direction towards the final result of the rotation transformation. It is input by user.
		//It does not need to be normalized
		std::cout << "Using right-handed coordinate system, creating a delta rotation matrix towards"
            "\nan arbitrary direction from the world space direction (absolute camera facing direction)\n " << std::string(fromDirection) << std::endl;
		std::cout << "The matrix before rotation transformation is:\n" << std::string(unmodifiedMatrix) << std::endl;
		std::cout << "Input the x, y and z coordinates of the \"to\" (absolute camera target position) direction:" << std::endl;

		std::cout << "X: ";
		std::cin >> inputToDirection.x;
		std::cout << std::endl;
		std::cout << "Y: ";
		std::cin >> inputToDirection.y;
		std::cout << std::endl;
		std::cout << "Z: ";
		std::cin >> inputToDirection.z;
		std::cout << std::endl;

		Matrix3x3 rotationTransformationMatrix(Matrix3x3::LookAtRotationMatrix(fromDirection, inputToDirection));
		std::cout << "The constructed rotation transformation matrix is:\n" << std::string(rotationTransformationMatrix) << std::endl;
		Vector3 rotationTransformedVector = rotationTransformationMatrix * fromDirection;
		std::cout << "The transformed vector (new potential camera facing direction) is:\n" << std::string(rotationTransformedVector) << std::endl;

        // this operation can be faulty for situations where the target is at 0,0,1 or 0,0,-1
        Vector3 lookAtHorizon(rotationTransformedVector.x, rotationTransformedVector.y, 0);
        angle = Vector3::Angle(lookAtHorizon, rotationTransformedVector, Format::Angle::Degrees);
		if (angle > 45.0f)
		{
			std::cout << "Unfortunately the angle between " << std::string(lookAtHorizon) << " (horizon) and "
				<< std::string(rotationTransformedVector) << "(new potential facing direction),\n which is " << angle << " degrees, exceeded the 45 degrees barrier."
                "\n The camera is NOT able to keep track of the target." << std::endl;
        }
        else {
            std::cout << "Camera is following the target... \n";
            std::cout << "The angle between " << std::string(lookAtHorizon) << " (horizon) and "
                << std::string(rotationTransformedVector) << "\n(new potential facing direction) is " << angle << " degrees." << std::endl;
        }
	}
    char anyKey;
    std::cin >> anyKey;
}