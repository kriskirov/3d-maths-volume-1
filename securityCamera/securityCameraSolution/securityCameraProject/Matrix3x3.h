#ifndef Matrix3x3_h
#define Matrix3x3_h

#include "Vector3.h"
#include <string>

class Matrix3x3
{
public:
    Matrix3x3();
    Matrix3x3(const Vector3& p_col1, const Vector3& p_col2, const Vector3& p_col3);
    ~Matrix3x3();

    static Matrix3x3 Identity();
    static Matrix3x3 XRotationMatrix(const Vector3& from, const Vector3& to);
    static Matrix3x3 YRotationMatrix(const Vector3& from, const Vector3& to);
    static Matrix3x3 ZRotationMatrix(const Vector3& from, const Vector3& to);

    static Matrix3x3 LookAtRotationMatrix(const Vector3& from, const Vector3& to);
    void LookAt(const Vector3& lookAtPos, const Vector3& upVec);

    Vector3 operator*(const Vector3& rhs) const;
    Matrix3x3 operator*(const Matrix3x3& rhs) const;
    inline operator std::string() const {
        
        return 
            "|" + std::to_string(col1.x) + "|" + std::to_string(col2.x) + "|" + std::to_string(col3.x) + "|" + "\n" +
            "|" + std::to_string(col1.y) + "|" + std::to_string(col2.y) + "|" + std::to_string(col3.y) + "|" + "\n" +
            "|" + std::to_string(col1.z) + "|" + std::to_string(col2.z) + "|" + std::to_string(col3.z) + "|" + "\n" ;
            
    }

    Vector3 GetXBasis() const;//X Column
    Vector3 GetYBasis() const;//Y Column
    Vector3 GetZBasis() const;//Z Column

    Vector3 GetXRow() const;
    Vector3 GetYRow() const;
    Vector3 GetZRow() const;

    Vector3 col1, col2, col3;
};

#endif // !Matrix3x3_h

