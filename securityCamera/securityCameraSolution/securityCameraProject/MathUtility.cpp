#include "MathUtility.h"
#include <cmath>// include only atan?
#include <math.h>

//#define USE_MATH_DEFINES
//const double PI M_PI;

float PI()
{
    static float pi = atan(1) * 4;// just use the math.h version - G.Popov
    return pi;
}

//}

float Sign(float x)
{
	if(x > 0) return 1.0f;
	else if (x < 0) return -1.0f;
	else return 0.0f;
}
