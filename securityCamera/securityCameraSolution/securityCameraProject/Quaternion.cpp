#include "Quaternion.h"
#include "MathUtility.h"
//#include "Vector3.h"
#include <math.h>


Quaternion::Quaternion(const Vector3 & axis, const float degrees)
{
    float rads = (degrees / 360.0f) * PI() * 2.0f; //convert from degrees to rads
    // init the members
    w = cos(rads * 0.5f);
    v.x = axis.x * sin(rads * 0.5f);
    v.y = axis.y * sin(rads * 0.5f);
    v.z = axis.z * sin(rads * 0.5f);
}

Quaternion::Quaternion() : w(0), v(0,0,0)
{
}

Quaternion::~Quaternion()
{
}

Quaternion Quaternion::operator*(const Quaternion& rhs) const
{
    Quaternion r;

    r.w = w*rhs.w - v.Dot(rhs.v);
    r.v = v*rhs.w + rhs.v*w + v.Cross(rhs.v);

    return r;
}

Vector3 Quaternion::operator*(const Vector3& rhs) const
{
    Quaternion p;
    
    p.w = 0;
    p.v = rhs;

    const Quaternion& q = (*this);
    
    return (q * p * q.Inverted()).v; // this is not optimized
}

Quaternion::operator std::string() const
{
    return "<" + std::to_string(w) + ", " + std::to_string(v.x) + ", " + std::to_string(v.y) + ", " + std::to_string(v.z) + ">";
}

Quaternion Quaternion::Inverted() const
{
    Quaternion q;
    
    q.w = w;
    q.v.x = -v.x;
    q.v.y = -v.y;
    q.v.z = -v.z;

    return  q;
}