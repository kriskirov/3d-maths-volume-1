#ifndef Quaternion_h
#define Quaternion_h

#include "Vector3.h"
#include <string>

class Quaternion
{
public:
    Quaternion();
    Quaternion(const Vector3& axis, const float degrees);
    ~Quaternion();

    Quaternion operator*(const Quaternion& rhs) const;
    Vector3 operator*(const Vector3& rhs) const;
    operator std::string() const;

    Quaternion Inverted() const;
    
    float w;
    Vector3 v;
};

#endif // !Quaternion_h

